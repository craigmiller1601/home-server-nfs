#!/bin/bash

set -e

TAG=nexus-docker.craigmiller160.us/nfs-reader:1

docker build \
  --platform=linux/amd64 \
  -t "$TAG" \
  .

docker push "$TAG"