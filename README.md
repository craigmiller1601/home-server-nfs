# Home Server NFS

This provides an NFS server for the kubernetes cluster.

## Install the Driver

Make sure the `install-nfs-driver.sh` has been executed to install the NFS driver in the cluster.