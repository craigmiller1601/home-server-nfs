#!/bin/bash

set -e

helm install \
  csi-driver-nfs csi-driver-nfs/csi-driver-nfs \
  --namespace kube-system \
  --set kubeletDir=/var/snap/microk8s/common/var/lib/kubelet

kubectl wait \
  pod \
  --selector app.kubernetes.io/name=csi-driver-nfs \
  --for condition=ready \
  --namespace kube-system